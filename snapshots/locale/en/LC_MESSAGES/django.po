# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-10-06 08:43+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n"
"%100>=11 && n%100<=14)? 2 : 3);\n"

#: .\snapshots\templates\snapshots\snapshot.html:11
msgid "snapshot_name"
msgstr "Name"

#: .\snapshots\templates\snapshots\snapshot.html:12
msgid "snapshot_createdAt"
msgstr "Created at"

#: .\snapshots\templates\snapshots\snapshot.html:13
msgid "snapshot_dataStateAt"
msgstr "Data actualized on"

#: .\snapshots\templates\snapshots\snapshot.html:14
msgid "snapshot_actions"
msgstr "Actions"

#: .\snapshots\templates\snapshots\snapshot.html:25
#: .\snapshots\templates\snapshots\snapshot.html:30
msgid "snapshot_create_clone"
msgstr "Create clone from this snapshot"
