from django.contrib import auth
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _
from django.contrib.auth import authenticate

from .models import Group, Permissions


def user_has_permission(user: User, permission: str):
    if user.is_superuser:
       return True
    permissions = set()
    user_groups = Group.objects.filter(user=user)
    for group in user_groups:
        permissions = permissions.union(group.group.role.permission.all())
    return Permissions.objects.get(permission=permission) in permissions


def check_auth(request):
    if request.user.is_authenticated and request.user.is_active:
        if request.path_info in ('/', '/login'):
            return redirect('/status')
    return user_login(request)


def show_auth_page(request, is_error=False):
    return render(request, 'users/login.html',
                  {"title": _('authentication'), "is_error": is_error})


def user_login(request):
    if request.method == 'GET' or 'user_login' not in request.POST or 'user_password' not in request.POST:
        return show_auth_page(request, False)
    username = request.POST.get('user_login')
    password = request.POST.get('user_password')
    if len(str(username).strip()) < 2 or len(str(password).strip()) < 1:
        return show_auth_page(request, True)
    user = authenticate(username=username, password=password)
    if user is not None and user.is_active:
        # Правильный пароль и пользователь "активен"
        auth.login(request, user)
        # Перенаправление на "правильную" страницу
        return redirect('/status')
    else:
        # Отображение страницы с ошибкой
        return show_auth_page(request, True)


def user_logout(request):
    auth.logout(request)
    # Перенаправление на страницу.
    return redirect("/login")
