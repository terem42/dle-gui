from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns = [
    path('', views.check_auth),
    path('login', views.check_auth),
    path('logout', views.user_logout),
]