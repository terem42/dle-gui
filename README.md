# Database Lab Engine (DLE) GUI

Database Lab Engine (DLE) - открытый движок для создания тонких клонов БД.
На текущий момент работает с СУБД PostgreSQL. "Тонкое" клонирование работает за счёт 
снимков ZFS (основной бакэнд) или LVM. Позволяет очень быстро создать обособленную 
копию БД и работать с ней как с отдельной сущностью, не затрагивая основную БД.

Сайт - https://postgres.ai/

Бесплатно распространяется только основной движок. Графическое окружение, позволяющее
работать с DLE более удобным способом является платным и предоставляется как SaaS 
за отдельную плату (https://postgres.ai/pricing) 

Данное ПО является графической надстройкой над API DLE (https://gitlab.com/postgres-ai/database-lab/-/blob/master/api/swagger-spec/dblab_server_swagger.yaml)

> **⚠ DLE GUI не разрабатывается Postgres.ai и не является официальным UI для DLE. Это сторонняя надстройка**

## Лицензия
[Читать](./LICENSE.md)

## Возможности
 - Отображение статуса DLE и статистики по файловой системе
 - Отображение списка снашотов с возможностью быстро создать клон на основе любого снапшота
 - Отображение списка клонов с возможностью:
      - Снять/Установить признак "protected" (при protected=true клон не будет удалён, в случае если по нему 
        не было активности более заданного в файле конфигурации числа минут
      - Сбросить все изменения в клоне на начальное состояние
      - Удалить клон
 - Ограничение доступа к DLE GUI
 - Разграничение доступа на основе привилегий

SaaS от [postgres.ai](https://postgres.ai/) имеет ещё интеграцию с Joe Bot, историю и другие приятности. 
Тут этого нет. Как говорится: "Первая доза бесплатно" ;)

## Картинки

![status](./doc/snapshots.png)
![status](./doc/status.png)
![status](./doc/create-clone.png)

## TODO
 - [ ] Логирование действий пользователей
      - [x] Создание клона
      - [x] Сброс клона  
      - [x] Установка/снятие защиты для клона
      - [x] Удаление клона
      - [ ] Создание снимка
 - [x] Привилегии
      - [x] Просмотр списка клонов, созданных этим пользователем
      - [x] Просмотр списка всех клонов  
      - [x] Создание нового клона
      - [x] Сброс клона, созданного этим пользователем
      - [x] Сброс любого клона  
      - [x] Установка/снятие защиты для клонов, созданных этим пользователем
      - [x] Установка/снятие защиты для любого клона
      - [x] Удаление клона, созданного этим пользователем
      - [x] Удаление любого клона
      - [x] Создание снапшота по требованию (после добавление данной возможности в API DLE)
 - [x] Роли для пользователей
      - [x] Администратор
        - [x] Просмотр списка всех клонов
        - [x] Создание нового клона
        - [x] Сброс любого клона
        - [x] Удаление любого клона
        - [x] Установка/снятие защиты для любого клона
        - [x] Создание снапшотов по требованию (после добавление данной возможности в API DLE)
      - [x] Оператор
        - [x] Просмотр списка всех клонов
        - [x] Создание нового клона
        - [x] Сброс любого клона
        - [x] Удаление клона, созданного этим пользователем
        - [x] Установка/снятие защиты для клонов, созданных этим пользователем
        - [x] Создание снапшотов по требованию (после добавление данной возможности в API DLE)
      - [x] Пользователь
         - [x] Просмотр списка клонов, созданных этим пользователем
         - [x] Создание нового клона
         - [x] Сброс клона, созданного этим пользователем
         - [x] Удаление клона, созданного этим пользователем
         - [x] Установка/снятие защиты для клонов, созданных этим пользователем
      - [x] Наблюдатель
         - [x] Просмотр списка всех клонов
 - [ ] Создание снапшота по требованию (после добавления такой возможности в API DLE. Сейчас снапшоты создаются только по расписанию, заданному в файле конфигурации)
 - [ ] Авторизация через Keycloak


## entrypoint.sh
Скрипт начального запуска содержит команды:
1. start - запуск DLE GUI
2. migrate - выполнение миграций
3. init - инициализация приложения (миграции, создание суперпользователя, загрузка начальных данных: лействий с клонами, привилегий, ролей, групп)
4. mstart - применение всех действий из init (загрузка начальных данных отдельно регулируется переменной $DG_LOAD_INITIAL_DATA) перед запуском приложения


## Запуск

По умолчанию переменная DG_LOAD_INITIAL_DATA установлена в значение false, чтобы при каждом запуске не грузить 
стартовые настройки. Поэтому при первом запуске нужно запустить DLE GUI с параметром init.

### Из исходного кода
1. Установить все необходимые зависимости
   ``` bash
   # pip install -r requirements.txt
   ```
2. Скопировать `.env-template` в `.env`, прописать нужные параметры и импортировать переменные `sourсe .env`
3. Запустить скрипт ./entrypoint.sh с параметрами:
   1. init
   2. mstart
   ``` bash
   $ python manage.py init
   $ python manage.py mstart
   ```

### С помощью Docker-контейнера
1. Сборка
   ``` bash
   $ docker build -t dlegui:v0.1.0 -f Dockerfile .
   ```
2. Запуск
   ``` bash
   $ cp .env-template .env
   $ echo "Прописываем в .env нужные значения"
   $ source .env
   $ docker run -it --rm --name dlegui -e DG_DATABASE_NAME=$DG_DATABASE_NAME -e DG_DATABASE_USER=$DG_DATABASE_USER -e DG_DATABASE_PASSWORD=$DG_DATABASE_PASSWORD -e DG_DATABASE_HOST=$DG_DATABASE_HOST -e DG_DATABASE_PORT=$DG_DATABASE_PORT -e DG_DBLAB_TOKEN=$DG_DBLAB_TOKEN -e DG_DBLAB_URL=$DG_DBLAB_URL -e DG_ADMIN_USERNAME=$DG_ADMIN_USERNAME -e DG_ADMIN_PASSWORD=$DG_ADMIN_PASSWORD -e DG_ADMIN_EMAIL=$DG_ADMIN_EMAIL -e DG_SECRET_KEY=$DG_SECRET_KEY -e DG_LOAD_INITIAL_DATA=$DG_LOAD_INITIAL_DATA -p 8000:8000 dlegui:v0.1.0 init
   $ docker run -it --rm --name dlegui -e DG_DATABASE_NAME=$DG_DATABASE_NAME -e DG_DATABASE_USER=$DG_DATABASE_USER -e DG_DATABASE_PASSWORD=$DG_DATABASE_PASSWORD -e DG_DATABASE_HOST=$DG_DATABASE_HOST -e DG_DATABASE_PORT=$DG_DATABASE_PORT -e DG_DBLAB_TOKEN=$DG_DBLAB_TOKEN -e DG_DBLAB_URL=$DG_DBLAB_URL -e DG_ADMIN_USERNAME=$DG_ADMIN_USERNAME -e DG_ADMIN_PASSWORD=$DG_ADMIN_PASSWORD -e DG_ADMIN_EMAIL=$DG_ADMIN_EMAIL -e DG_SECRET_KEY=$DG_SECRET_KEY -e DG_LOAD_INITIAL_DATA=$DG_LOAD_INITIAL_DATA -p 8000:8000 dlegui:v0.1.0
   ```
> **⚠ DLE GUI тестировался только с БД PostresSQL.**

При запуске контейнера выполняются следующие задачи:
 - Подключение к БД и запуск миграций
 - Создание суперпользователя, указанного в переменных окружения DG_ADMIN_*
   - по умолчанию DG_ADMIN_USERNAME=admin, DG_ADMIN_PASSWORD=admin, DG_ADMIN_EMAIL=admin@domain.local (прописано в entrypoint.sh)
   - если пользователь существует, то он не будет пересоздаваться или обновляться
     > **⚠ После первого запуска обязательно нужно сменить пароль суперпользователю**
  - Запуск сервера на порту 8000  

## Настройка приложения
[Читать](./doc/CONFIGURING.md)

## Для разработчиков
### Локализация
1. В начале шаблона прописать
     ``` jsregexp
   {% load i18n %}
   ```  
2. В тесте шаблона прописать
     ``` html
   <th scope="col">{% trans 'clone_count' %}</th>
   ```  

3. Войти в нужное "приложение"
    ``` bash
   $ cd clones
   ```
4. Пропарсить все шаблоны и занести в файл перевода .po все строки-шаблоны
    ``` bash
   $ django-admin makemessages -l ru
   ```
5. Открыть файл locale/ru/LC_MESSAGES/django.po, найти там добавленную 
   строку `msgid "clone_diff_size"` и в `msgstr` прописать перевод. 
   Если перед блоком есть комментарий вида `#, fuzzy`, то его нужно удалить
    ``` bash
    #: templates/clones/clone_list.html:45
    msgid "clone_diff_size"
    msgstr "Занимаемое место"
   ```
6. Скомпилировать тестовый файл перевода (.po) в бинарный (.mo)
    ``` bash
    $ django-admin compilemessages | grep "locale/ru"
   ```
